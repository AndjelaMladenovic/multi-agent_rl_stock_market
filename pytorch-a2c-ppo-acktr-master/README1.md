# pytorch-a2c-ppo-acktr

This is a PyTorch implementation of
* Advantage Actor Critic (A2C), a synchronous deterministic version of [A3C](https://arxiv.org/pdf/1602.01783v1.pdf)
* Proximal Policy Optimization [PPO](https://arxiv.org/pdf/1707.06347.pdf)
* Scalable trust-region method for deep reinforcement learning using Kronecker-factored approximation [ACKTR](https://arxiv.org/abs/1708.05144)

Also see the OpenAI posts: [A2C/ACKTR](https://blog.openai.com/baselines-acktr-a2c/) and [PPO](https://blog.openai.com/openai-baselines-ppo/) for more information.

This implementation is inspired by the OpenAI baselines for [A2C](https://github.com/openai/baselines/tree/master/baselines/a2c), [ACKTR](https://github.com/openai/baselines/tree/master/baselines/acktr) and [PPO](https://github.com/openai/baselines/tree/master/baselines/ppo1). It uses the same hyper parameters and the model since they were well tuned for Atari games.



## Requirements
Described in README.md

In order to install requirements, follow instructions in README.md.
```
Additionally to that, make sure you add path to the custom multitrading environment in your python path.

## Training command


#### PPO

```bash
python main.py --env-name 'tradingmulti-v0' --algo ppo --use-gae --lr 2.5e-4 --clip-param 0.1 --value-loss-coef 0.5 --num-processes 8 --num-steps 128 --num-mini-batch 4 --log-interval 1 --use-linear-lr-decay --use-linear-clip-decay --entropy-coef 0.01 
```

#### PPO

```

## Explantion of the code and important files:

arguments.py - File that contains parser for different type of arguments. Useful to see what is the default set up for the parameters that are not defined by user.

main.py:
- directories for saving files are defined
- initialize actor critic network(s)
- vector of environments is intiliazed
- agent is defined and what algorithm uses to learn (PPO, a2c or acktr)
- Then we have two nested loops:
  - inside of range(num_updates):
    - inside of range(num_steps:
      values, actions and probabilities are returned for all environments
      repeated for num_steps
    the actual update happends (because of the update of the policy network happends always between num_steps)
  finish the training
 - evaluation

model.py -
Policy class is defined. Inside of it, different type of networks are defined. In the model we are using three distributions to sample action space. 
We assume that the random variables that represent price, amount and actual action are all indepenedent random variables. (Not correct assumption, but the simplest one at this point). Actual action is sampled from categorical distribution (0,1,2 -buy,sell, hold), while amount in the action and the price are sampled from two independent Gaussian distributions.

storage.py - 
Defines class for data structure rolling storage for storing data. This is used to store data used for update in the policy network. (num_steps x num_processes x obs_shape x action_space)

distributions.py -
different distribution classes are defined here (Gaussian, Categorical, Bernoulli). Note that in these classes we can use sample, mean etc functions that are defined and imported from pytorch for these distributions.

ppo.py - The actual proximal policy optimization algorithm. It is implemented exactly like described in paper. Please refer to https://arxiv.org/pdf/1707.06347.pdf).


envs.py - vector of environments are made inside of this file. Note, reward  of the environments is normalized here. Based on rolling average (mean substracted, divided by deviation). It might be useful to know for later.

tradingmulti_env.py:
- Class Offer and Demand defined (how we basically define the offer or demand request and agent makes - price, amount, who is selling/buying, and what stock)
- Class Trading Env - this is our environment.
NUM_STOCK - total number of the stocks on the market
NUM_AGENTS_all - total number of learned and hardcoded agents
NUM_AGENTS - total number of learned agents
money_start - how much money each agent has at the begining of the simulation
done - when the environment should restart, not really used currentyl though
round - round of trading
action_space - action space is defined with action buy/sell/hold represented as (0,1,2) and amount and price
observation_space - currently the observation space is each agent sees how much other agent poses stocks and money they have. Also what was the profit of the stock in the previous round and the amount of stock that got traded on the market. Please note that this observation space we will change soon.
- The actual trade is happening between learned agents(currently only one) and hardcoded agents. For the rules of the trade refer to the document posted on the wiki project page.
 





